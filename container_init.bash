until cd /app
do
    echo "waiting for docker-compose to bind the volume"
done

pip install -r requirements.txt

python manage.py makemigrations
python manage.py migrate
python manage.py runserver 0.0.0.0:8000

