FROM python:3.9-alpine

EXPOSE 8000

# RUN mkdir -p /app

# WORKDIR /app

# COPY . .

COPY container_init.bash .
RUN apk add postgresql-dev
RUN apk add build-base

# RUN pip install -r requirements.txt


# CMD python manage.py migrate; \
#     python manage.py makemigrations; \
#     python manage.py runserver 0.0.0.0:8000

CMD sh container_init.bash