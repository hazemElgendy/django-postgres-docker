

from django.urls import path
from . import views

urlpatterns = [
    path('clients/get/<id>', views.get),
    path('clients/edit/<id>', views.edit),
    path('clients/delete/<id>', views.delete),
    path('clients/create', views.create)

]
