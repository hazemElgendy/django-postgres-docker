# from django.shortcuts import render
import json
from django.http import JsonResponse,  HttpResponse, HttpResponseBadRequest
from django.forms import model_to_dict
from .models import Client
# Create your views here.

def get(request, id):
    client = Client.objects.get(id=id)

    return JsonResponse(model_to_dict(client))


def edit(request, id):
    if request.method == "POST":
        try:
            client = Client.objects.get(id=id)
            new_client = json.loads(request.body)
            client.first_name = new_client['first_name']
            client.last_name = new_client['last_name']
            client.date_of_birth = new_client['date_of_birth']
            client.save()
            return JsonResponse(model_to_dict(client))
        except Exception as e:
            return HttpResponseBadRequest(e)
    return HttpResponseBadRequest()


def create(request):
    if request.method == "POST":
        client = json.loads(request.body)
        Client(first_name = client['first_name'], last_name = client['last_name'], date_of_birth = client['date_of_birth']).save()
        
        return JsonResponse(client)


def delete(request, id):
    client = Client.objects.get(id=id)
    client.delete()
    return JsonResponse(model_to_dict(client))